import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.StringTokenizer;

/**
 * Created by Student on 07.05.2015.
 */
public class NetworkTest {
    public static void main(String[] args) throws SocketException {
        Enumeration<NetworkInterface>  interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface networkInterface =  interfaces.nextElement();
            System.out.println(networkInterface);
            System.out.println(networkInterface.getInterfaceAddresses());
            System.out.println(networkInterface.getDisplayName());
            System.out.println(networkInterface.getInetAddresses());
            System.out.println("\n");
        }

    }

}
