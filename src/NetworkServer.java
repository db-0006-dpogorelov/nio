import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Student on 07.05.2015.
 */
public class NetworkServer {
    public static void main(String[] args) throws IOException {

        try (ServerSocketChannel socket = ServerSocketChannel.open()) {
            socket.bind(new InetSocketAddress(50000));
            Selector selector = Selector.open();
            socket.configureBlocking(false);
            socket.register(selector, SelectionKey.OP_ACCEPT);

            while (true) {
                selector.select();
                Iterator<SelectionKey> iterator1 = selector.selectedKeys().iterator();
                while (iterator1.hasNext()) {
                    SelectionKey next = iterator1.next();
                    next.cancel();
                    ((ServerSocketChannel) next.channel()).accept();
                    System.out.println(next);
                    iterator1.remove();

                }
//                System.out.println("Nothing happend");
//                for (Iterator<SelectionKey> iterator = keys.iterator(); iterator.hasNext(); ) {
//                    SelectionKey next =  iterator.next();
//
//                }
            }
//            System.out.println("port: " + socket.getLocalPort());
//            System.out.println("inetadress: " + socket.getInetAddress());
//            System.out.println("localsocketadress:" + socket.getLocalSocketAddress());
//            while (true){
//                SocketChannel connection = socket.accept();
//                System.out.println(connection);
//            }
        }

    }
}
