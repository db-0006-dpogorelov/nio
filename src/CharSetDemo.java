import java.io.UnsupportedEncodingException;
import java.nio.charset.CharsetEncoder;
import java.util.Arrays;

/**
 * Created by Student on 07.05.2015.
 */
public class CharSetDemo {
    public static void testChars() throws UnsupportedEncodingException {
        System.out.println(Arrays.toString("������Hello".getBytes()));
        System.out.println(new String("������Hello".getBytes()));
        System.out.println(Arrays.toString("������Hello".getBytes("UTF-8")));
        System.out.println(new String("������Hello".getBytes("UTF-8")));
        System.out.println(Arrays.toString("������Hello".getBytes("UTF-16")));
        System.out.println(new String("������Hello".getBytes("UTF-16")));
        System.out.println(Arrays.toString("������Hello".getBytes("UTF-32")));
        System.out.println(new String("������Hello".getBytes("UTF-32")));
        System.out.println(Arrays.toString("������Hello".getBytes("cp866")));
        System.out.println(new String("������Hello".getBytes("cp866")));
        System.out.println(Arrays.toString("������Hello".getBytes("cp1251")));
        System.out.println(new String("������Hello".getBytes("cp1251")));
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        testChars();
    }
}
