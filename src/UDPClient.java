import java.io.IOException;
import java.net.*;

/**
 * Created by Student on 07.05.2015.
 */
public class UDPClient {
    public static void main(String[] args) throws IOException {
        byte[] dataBytes = new byte[70000];

        DatagramPacket packet = new DatagramPacket(dataBytes, dataBytes.length);
        packet.setSocketAddress(new InetSocketAddress("127.0.0.1",50000));
        DatagramSocket socket = new DatagramSocket();
        socket.send(packet);
    }
}
