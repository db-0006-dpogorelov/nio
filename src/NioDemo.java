import javafx.util.Pair;

import java.io.*;
import java.nio.channels.ByteChannel;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * Demo for NIO-lesson  on 07.05.2015.
 */
public class NioDemo {
    static String startDirectory = "C:\\Program Files";
//    static String homeDirectory = "C:\\Users\\Student";
    static String biggestFile = "C:\\Program Files\\Java\\jdk1.8.0_40\\jre\\lib\\rt.jar";

    static void method(PrintStream out) {
        out.println("Hello World");

        System.out.println(out.checkError());

    }

    static void exceptionInOutputTest() {
        method(System.out);
        method(new PrintStream(new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                throw new IOException();
            }
        }));
    }

    static long readByByteTest(InputStream bigFile) throws IOException {
        long sum = 0;
        int oneByte;
        while ((oneByte = bigFile.read()) >= 0) {
            sum +=oneByte;
        }
        return  sum;
    }

    static void readSolidFile(long fileSize, InputStream bigFileStream) throws IOException {
        byte[] bytes = new byte[(int) fileSize];
        System.out.println(bigFileStream.read(bytes));

    }

    static Pair<Long, String> findBiggestFile(File currentDirectory) {
        if (currentDirectory.isFile()) {
            return new Pair<Long, String>(currentDirectory.length(), currentDirectory.getPath());
        } else {
            File[] files = currentDirectory.listFiles();
            Pair<Long, String> biggest = new Pair<Long, String>((long) -1, "");
            if (files != null) {
                for (File file : files) {
                    Pair<Long, String> current = findBiggestFile(file);
                    if (current.getKey() > biggest.getKey()) {
                        biggest = current;

                    }
                }
            } else {
                System.out.println("Null File array returned from " + currentDirectory.getPath());
            }

            return biggest;
        }
    }

    public static void main(String[] args) throws IOException {

//        biggestFile = findBiggestFile(new File(startDirectory)).getValue();
//        exceptionInOutputTest();
        long fileSize = new File(biggestFile).length();
        System.out.println(fileSize);
//        InputStream bigFileStream = new BufferedInputStream(new FileInputStream(biggestFile));
//        System.out.println(readByByteTest(bigFileStream));
//        readSolidFile(fileSize, bigFileStream);


        SeekableByteChannel channel = Files.newByteChannel(new File(biggestFile).toPath());
    }
}
