import java.io.IOException;
import java.net.*;

/**
 * Created by Student on 07.05.2015.
 */
public class UDPServer {
    public static void main(String[] args) throws IOException {
        MulticastSocket socket = new MulticastSocket(8888);
        socket.joinGroup(InetAddress.getByName("224.15.05.07"));
        byte[] dataBytes = new byte[30000];
        DatagramPacket packet = new DatagramPacket(dataBytes,dataBytes.length);
        while (true) {
            socket.receive(packet);
            System.out.println(packet.getAddress());
        }
    }
}
